require 'heb412_gen/version'

Msip.setup do |config|
  config.ruta_anexos = ENV.fetch('MSIP_RUTA_ANEXOS', 
                                 "#{Rails.root}/archivos/anexos")
  config.ruta_volcados = ENV.fetch('MSIP_RUTA_VOLCADOS',
                                   "#{Rails.root}/archivos/bd")
  # En heroku los anexos son super-temporales
  if !ENV["HEROKU_POSTGRESQL_GREEN_URL"].nil?
    config.ruta_anexos = "#{Rails.root}/tmp/"
  end
  config.titulo = "Heb412 - #{Heb412Gen::VERSION}"
  config.descripcion = "Nube y directorio de contactos"
  config.codigofuente = "https://gitlab.com/pasosdeJesus/si_fup"
  config.urlcontribuyentes = "https://gitlab.com/pasosdeJesus/si_fup/-/graphs/main?ref_type=heads"
  config.urlcreditos = "https://gitlab.com/pasosdeJesus/si_fup/-/blob/main/CREDITOS.md"
  config.agradecimientoDios = "<p>
  Agradecemos a Dios por su palabra y por permitir este desarrollo, el cual 
  le dedicamos.
  </p>
<blockquote>
Porque la palabra de Dios es viva y eficaz, y más cortante que toda 
espada de dos filos; y penetra hasta partir el alma y el espíritu, las 
coyunturas y los tuétanos, y discierne los pensamientos y las intenciones 
del corazón.
<br>
Hebreos 4:12
<blockquote>".html_safe


  config.colorom_fondo = '#ffffff'
  config.colorom_color_fuente = '#021324'
  config.colorom_nav_ini = '#00529d'
  config.colorom_nav_fin = '#00529d'
  config.colorom_nav_fuente = '#ffffff'
  config.colorom_fondo_lista = '#00b4a5'
  config.colorom_color_flota_subitem_fuente = '#ffffff'
  config.colorom_color_flota_subitem_fondo = '#00b4a5'
  config.colorom_btn_primario_fondo_ini = '#00529d'
  config.colorom_btn_primario_fondo_fin = '#00529d'
  config.colorom_btn_primario_fuente = '#ffffff'
  config.colorom_btn_peligro_fondo_ini = '#a62424'
  config.colorom_btn_peligro_fondo_fin = '#a62424'
  config.colorom_btn_peligro_fuente = '#ffffff'
  config.colorom_btn_accion_fondo_ini = '#00b4a5'
  config.colorom_btn_accion_fondo_fin= '#00b4a5'
  config.colorom_btn_accion_fuente = '#ffffff'
  config.colorom_alerta_exito_fondo = '#00b4a5'
  config.colorom_alerta_exito_fuente = '#f2fff2'
  config.colorom_alerta_problema_fondo = '#f8d7da'
  config.colorom_alerta_problema_fuente = '#721c24'
end
