class Ability  < Heb412Gen::Ability


  # Autorizacion con CanCanCan
  def initialize(usuario = nil)
    initialize_mr519_gen(usuario)

    if usuario && usuario.rol
      can(:read, Heb412Gen::Doc)
      can(:read, Heb412Gen::Plantilladoc)
      can(:read, Heb412Gen::Plantillahcm)
      can(:read, Heb412Gen::Plantillahcr)

      case usuario.rol
      when Ability::ROLANALI
        can([:create, :update, :destroy, :edit] , Heb412Gen::Doc)

      when Ability::ROLADMIN, Ability::ROLDIR
        can(:manage, Heb412Gen::Doc)
        can(:manage, Heb412Gen::Plantilladoc)
        can(:manage, Heb412Gen::Plantillahcm)
        can(:manage, Heb412Gen::Plantillahcr)
        can(:manage, Heb412Gen::Carpetaexclusiva)
      end
    end
  end # initialize

end # class
